import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by tadehb on 6/30/2017.
 */
public class EDDM {

    WebDriver driver;
    setDataDriven data;

    private String browser;
    private String platform;

    @BeforeClass
    public void BeforeClass() throws IOException {

        data = new setDataDriven();
        data.props();
        browser = data.getBrowser();
        platform = data.getPlatform();


        switch (browser) {

            case "firefox": {
                if (platform.equals("windows")) {
                    System.setProperty("webdriver.gecko.driver", "C:\\projects\\Ultra_Regrestion\\src\\test\\java\\drivers\\windows\\geckodriver.exe");
                } else {
                    System.setProperty("webdriver.gecko.driver", "./src/test/java/drivers/mac/geckodriver");
                }
                driver = new FirefoxDriver();
                break;
            }
            case "chrome": {
                if (platform.equals("windows")) {
                    System.setProperty("webdriver.chrome.driver", "C:\\projects\\Ultra_Regrestion\\src\\test\\java\\drivers\\windows\\chromedriver.exe");
                } else {
                    System.setProperty("webdriver.chrome.driver", "./src/test/java/drivers/mac/chromedriver");
                }
                driver = new ChromeDriver();
                break;
            }

            case "safari": {
            }

        }

        driver.get(data.getEnvironment());
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

    }

    @Test
    public void Login() throws InterruptedException {


        WebElement Username = driver.findElement(By.xpath(".//input[@id=\"txtEmail\"]"));
        Username.sendKeys(data.getUsername());
        Thread.sleep(2000);
        WebElement Password = driver.findElement(By.xpath(".//input[@id=\"txtPassword\"]"));
        Password.sendKeys(data.getPassword());
        Thread.sleep(2000);
        WebElement SignIn = driver.findElement(By.xpath(".//input[@id=\"btnLogin\"]"));
        SignIn.click();
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//button[@id=\"btnCart\"]")));

    }

    @Test(dependsOnMethods = {"Login"})
    public void EDDMFlyer() throws InterruptedException {


        driver.findElement(By.xpath(".//*[@id='leftNav']//a[text()='Flyers']")).click();
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='builder-options']/form/div[1]/label")));
        WebElement ServiceOption = driver.findElement(By.xpath(".//select[@id='facet-038bcdd0-0e1a-4de4-ad8c-2c4cc0df7d56']"));
        Select sc1 = new Select(ServiceOption);
        sc1.selectByIndex(1);
        Thread.sleep(2000);
        WebElement size = driver.findElement(By.xpath(".//*[@id='facet-34f407f8-0b50-4227-9378-10fddefbe596']"));
        Select sc2 = new Select(size);
        sc2.selectByIndex(1);
        Thread.sleep(2000);
        WebElement coating = driver.findElement(By.xpath(".//*[@id='facet-26ca0df3-0682-4f37-8979-409868e2df2d']"));
        Select sc3 = new Select(coating);
        sc3.selectByIndex(1);
        Thread.sleep(2000);
        WebElement colorspec = driver.findElement(By.xpath(".//*[@id='facet-38d33954-5a42-4112-a905-215eb827e62c']"));
        Select sc4 = new Select(colorspec);
        sc4.selectByIndex(1);
        Thread.sleep(5000);
        WebElement Dates = driver.findElement(By.xpath(".//input[@id='deliveryDate']"));
        Dates.click();
        Thread.sleep(2000);
        WebElement days = driver.findElement(By.className("clndr-grid-days"));
        Thread.sleep(2000);
        List<WebElement> activedays = days.findElements(By.xpath("//div[contains(@class, 'clndr-grid-item') and contains(@class, 'clndr-day') and  not (contains(@class, 'clndr-inactive'))]"));

        activedays.get(0).click();
        Thread.sleep(800);
        WebElement SelectRoutesButton = driver.findElement(By.xpath("//button[@id='btnNext']"));
        SelectRoutesButton.click();
        WebDriverWait wait2 = new WebDriverWait(driver, 60);
        wait2.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='leftNav']//a[text()='Flyers']")));
        Thread.sleep(2000);
        driver.switchTo().frame("foEddmMap");
        WebElement EnterZipCode = driver.findElement(By.id("leaflet-control-geosearch-qry"));
        EnterZipCode.sendKeys("91205");
        Thread.sleep(500);
        driver.findElement(By.xpath(".//*[@id='foEddmMap']//button")).click();
        WebDriverWait wait3 = new WebDriverWait(driver, 60);
        wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//input[@id='leaflet-control-geosearch-qry']")));
        Thread.sleep(7000);
        driver.findElement(By.xpath(".//*[@id='EddmMap']//div[text()='91205']")).click();
        Thread.sleep(7000);
        driver.findElement(By.xpath(".//*[@id='EddmMap']//div[text()='Select All Routes']")).click();
        Thread.sleep(800);
        String total = driver.findElement(By.xpath(".//*[@id='foEddmMap']/div/div[3]/div/div/div[1]/div[3]/div/div/div/dl/dd[4]")).getText();
        Assert.assertNotEquals(total, " - - ", "Routes were not selected");
        driver.findElement(By.xpath(".//*[@id='btnNext']")).click();
        driver.switchTo().defaultContent();
        WebDriverWait wait4 = new WebDriverWait(driver, 60);
        wait4.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//button[@id='btnNext'][text()='Continue to Cart']")));
        driver.findElement(By.xpath(".//*[@id='main-page-content']//input[contains(@class, 'upload-input dropzone')]")).click();


        Thread.sleep(10000);


    }


    @AfterClass
    protected void After_Class() throws InterruptedException {

        Thread.sleep(500);
        driver.close();
    }

}
