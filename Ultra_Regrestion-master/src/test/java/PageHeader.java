import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by tadeh on 6/22/17.
 */
public class PageHeader {


    private WebDriver driver;
    private setDataDriven data;
    private String browser;
    private String platform;


    @BeforeClass
    public void BeforeClass() throws IOException {

        data = new setDataDriven();
        data.props();
        browser = data.getBrowser();
        platform=data.getPlatform();

        switch(browser) {

            case "firefox": {
                if (platform.equals("windows")) {
                    System.setProperty("webdriver.gecko.driver", "C:\\projects\\Ultra_Regrestion\\src\\test\\java\\drivers\\windows\\geckodriver.exe");
                } else {
                    System.setProperty("webdriver.gecko.driver", "./src/test/java/drivers/mac/geckodriver");
                }
                driver = new FirefoxDriver();
                break;
            }
            case "chrome": {
                if (platform.equals("windows")) {
                    System.setProperty("webdriver.chrome.driver", "C:\\projects\\Ultra_Regrestion\\src\\test\\java\\drivers\\windows\\chromedriver.exe");
                } else {
                    System.setProperty("webdriver.chrome.driver", "./src/test/java/drivers/mac/chromedriver");
                }
                driver = new ChromeDriver();
                break;
            }

            case "safari": {
            }


        }

        driver.get(data.getEnvironment());
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        }


    @Test
    public void LogoIsEnable() {

        SoftAssert so = new SoftAssert();

        boolean LogoIsEnable = driver.findElement(By.xpath(".//*[@id='main-header']/div/header/div[2]/h1/a")).isEnabled();
        so.assertEquals(LogoIsEnable, true, "Logo is missing on the header");

        String LogoLinkTagName = driver.findElement(By.xpath(".//*[@id='main-header']/div/header/div[2]/h1/a")).getTagName();
        so.assertEquals(LogoLinkTagName, "a", "Logo's is not 'a' tag");

        String LogoHyperLink = driver.findElement(By.xpath(".//*[@id='main-header']/div/header/div[2]/h1/a")).getAttribute("href");
        so.assertEquals(LogoHyperLink, driver.getCurrentUrl() + "#home", "Logo's Hyper link is wrong");

        String LogoValue = driver.findElement(By.xpath(".//*[@id='main-header']/div/header/div[2]/h1/a")).getText();
        so.assertEquals(LogoValue, "4over.com", "the value of Logo is wrong");

        so.assertAll();


    }


    @Test
    public void VideoLink() {

        SoftAssert so = new SoftAssert();

        boolean VideoLink = driver.findElement(By.xpath(".//*[@id='main-header']/div/header/div[2]/a")).isEnabled();
        so.assertEquals(VideoLink, true, "VideoLink is missing on the header");

        String VideoLinkTagName = driver.findElement(By.xpath(".//*[@id='main-header']/div/header/div[2]/a")).getTagName();
        so.assertEquals(VideoLinkTagName, "a", "Video Link's is not 'a' tag");

        String VideoLinkHyperLink = driver.findElement(By.xpath(".//*[@id='main-header']/div/header/div[2]/a")).getAttribute("href");
        so.assertEquals(VideoLinkHyperLink, "https://www.youtube.com/embed/FtrdO0K3Z1A", "Video Link's Hyper link is wrong");

        String VideoLinkValue = driver.findElement(By.xpath(".//*[@id='main-header']/div/header/div[2]/a/span")).getText();

        so.assertEquals(VideoLinkValue, "2.0 Video", "Watch 4 2.0 Video");

        so.assertAll();

    }


    @Test
    public void ShopHeader() {


        SoftAssert so = new SoftAssert();

        boolean ShopHeader = driver.findElement(By.xpath(".//*[@id='main-header']/div/header/nav/div/div/div/nav[1]/h3")).isEnabled();
        so.assertEquals(ShopHeader, true, "Shop Header string is missing ");

        String ShopHeaderString = driver.findElement(By.xpath(".//*[@id='main-header']/div/header/nav/div/div/div/nav[1]/h3")).getText();
        so.assertEquals(ShopHeaderString, "SHOP", "Shop Header String is wrong");
        so.assertAll();


    }

    @Test
    public void ManageHeader() {


        SoftAssert so = new SoftAssert();

        boolean ManageHeader = driver.findElement(By.xpath(".//*[@id='main-header']/div/header/nav/div/div/div/nav[2]/h3")).isEnabled();
        so.assertEquals(ManageHeader, true, "Manage Header string is missing ");

        String ManageHeaderString = driver.findElement(By.xpath(".//*[@id='main-header']/div/header/nav/div/div/div/nav[2]/h3")).getText();
        so.assertEquals(ManageHeaderString, "MANAGE", "Manage Header String is wrong");
        so.assertAll();


    }


    @Test(description = "Email Input Validation")
    public void EmailInput() {

        SoftAssert so = new SoftAssert();

        boolean EmailInput = driver.findElement(By.xpath(".//*[@id='txtEmail']")).isEnabled();
        so.assertEquals(EmailInput, true, "Email input is corruptedg");

        String EmailType = driver.findElement(By.xpath(".//*[@id='txtEmail']")).getAttribute("type");
        so.assertEquals(EmailType, "email", "Email type attribiute is wrong ");
        so.assertAll();

    }


@Test
    public void PasswordInput(){

        SoftAssert so = new SoftAssert();
        boolean PasswordInput= driver.findElement(By.id("txtPassword")).isEnabled();
        so.assertEquals(PasswordInput,true,"Password input is corruptedd");

        String PasswordType = driver.findElement(By.id("txtPassword")).getAttribute("type");
        so.assertEquals(PasswordType, "password", "Password type attribiute is wrong ");
        so.assertAll();
    }

    @Test
    public void ForgotPassword(){

        SoftAssert so = new SoftAssert();
        boolean ForgotPassword= driver.findElement(By.xpath(".//*[@id='main-header']/div/header/div[2]/div/div/form/a[2]")).isEnabled();
        so.assertEquals(ForgotPassword,true,"ForgotPasswordLink is corruptedd");

        String ForgotPasswordLink= driver.findElement(By.xpath(".//*[@id='main-header']/div/header/div[2]/div/div/form/a[2]")).getAttribute("href");
        so.assertEquals(ForgotPasswordLink, driver.getCurrentUrl()+"#forgotpassword", "Forgot Password link is wrong ");
        String ForgotPasswordHyperLink = driver.findElement(By.xpath(".//*[@id='main-header']/div/header/div[2]/div/div/form/a[2]")).getText();
        so.assertEquals(ForgotPasswordHyperLink,"Forgot your password?","Forgot Password sting is wrong");
        so.assertAll();

}

    @AfterMethod
    public void After_Method(ITestResult testResult){

        if (testResult.getStatus() == ITestResult.FAILURE){

            System.out.println("Failed:  " +
                    "  " + testResult.getMethod().getMethodName());
        }
        if (testResult.getStatus() == ITestResult.SUCCESS){

            System.out.println("Success:  " + testResult.getMethod().getMethodName());
        }

    }


    @AfterClass
    public void After_Class() throws InterruptedException {
        Thread.sleep(500);
        driver.close();


    }
}
