import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by tadeh on 2/8/17.
 */
public class setDataDriven {

    private Properties props;

    private String username;
    private String password;
    private String environment;
    private String browser;
    private String filepath;

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    private String platform;


    public void props() throws IOException {

        props = new Properties();
        props.load(new FileInputStream("./src/test/java/dataDriven.properties"));
        username = props.getProperty("USERNAME");
        password = props.getProperty("PASSWORD");
        environment = props.getProperty("ENVIRONMENT");
        browser = props.getProperty("BROWSER");
        platform=props.getProperty("PLATFORM");
        filepath=props.getProperty("FILEPATH");


    }


    public Properties getProps() {
        return props;
    }

    public void setProps(Properties props) {
        this.props = props;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }
}
