import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
/**
 * Created by tadeh on 6/13/17.
 */
public class LeftNav {

    WebDriver driver;
    setDataDriven data;

    private String browser;
    private String platform;


    @BeforeClass
    public void BeforeClass() throws IOException {

        data = new setDataDriven();
        data.props();
        browser = data.getBrowser();
        platform = data.getPlatform();


        switch(browser) {

            case "firefox": {
                if (platform.equals("windows")) {
                    System.setProperty("webdriver.gecko.driver", "C:\\projects\\Ultra_Regrestion\\src\\test\\java\\drivers\\windows\\geckodriver.exe");
                } else {
                    System.setProperty("webdriver.gecko.driver", "./src/test/java/drivers/mac/geckodriver");
                }
                driver = new FirefoxDriver();
                break;
            }
            case "chrome": {
                if (platform.equals("windows")) {
                    System.setProperty("webdriver.chrome.driver", "C:\\projects\\Ultra_Regrestion\\src\\test\\java\\drivers\\windows\\chromedriver.exe");
                } else {
                    System.setProperty("webdriver.chrome.driver", "./src/test/java/drivers/mac/chromedriver");
                }
                driver = new ChromeDriver();
                break;
            }

            case "safari": {
            }

        }

        driver.get(data.getEnvironment());
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        }
    @Test
    public void Login() throws InterruptedException {


        WebElement Username = driver.findElement(By.xpath(".//input[@id=\"txtEmail\"]"));
        Username.sendKeys(data.getUsername());
        Thread.sleep(2000);
        WebElement Password = driver.findElement(By.xpath(".//input[@id=\"txtPassword\"]"));
        Password.sendKeys(data.getPassword());
        Thread.sleep(2000);
        WebElement SignIn = driver.findElement(By.xpath(".//input[@id=\"btnLogin\"]"));
        SignIn.click();
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//button[@id=\"btnCart\"]")));

    }


    @Test(dependsOnMethods = {"Login"})
    public void LeftNav_Title() {

        SoftAssert so = new SoftAssert();

        boolean products = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/a")).isEnabled();
        Assert.assertEquals(products, true);
        String products2 = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/a")).getText();
        Assert.assertEquals(products2, "Products");

    }


    @Test(dependsOnMethods = {"Login"}, groups = "Links")
    public void EDDMServices() {

        SoftAssert so = new SoftAssert();
        boolean EDDMServices_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[1]/button")).isEnabled();
        so.assertEquals(EDDMServices_isEnable, true,"EDDMService title is not available");
        String EDDmServices_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[1]/button")).getText();
        so.assertEquals(EDDmServices_getText, "EDDM Services", "EDDM Service string is wrong");

        boolean Flyer_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[1]/ul/li[1]/a")).isEnabled();
        so.assertEquals(Flyer_isEnable, true,"Flyer is not available");
        String Flyer_href = driver.findElement(By.xpath(".//*[@id='leftNav']//a[text()='Flyers']")).getAttribute("href");
        so.assertEquals(Flyer_href, driver.getCurrentUrl() + "#category/eddm/products/bde4fa99-4357-44bf-a0b0-5fc130a75c9d", "Flyer link is wrong");
        String Flyer_getText = driver.findElement(By.xpath(".//*[@id='leftNav']//a[text()='Flyers']")).getText();
        so.assertEquals(Flyer_getText, "Flyers", "Flyer string is wrong");


        boolean Postcards_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[1]/ul/li[2]/a")).isEnabled();
        so.assertEquals(Postcards_isEnable, true,"Postcards is not available");
        String Postcards_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[1]/ul/li[2]/a")).getAttribute("href");
        so.assertEquals(Postcards_href, driver.getCurrentUrl() + "#category/eddm/products/b4dfb8fe-ea23-4871-b3bb-9b6fd22e0e18", "Postcards link is wrong");
        String Postcards_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[1]/ul/li[2]/a")).getText();
        so.assertEquals(Postcards_getText, "Postcards", "Postcards string is wrong ");

        boolean SellSheets_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[1]/ul/li[3]/a")).isEnabled();
        so.assertEquals(SellSheets_isEnable, true,"SellSheets is not available");
        String SellSheets_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[1]/ul/li[3]/a")).getAttribute("href");
        so.assertEquals(SellSheets_href, driver.getCurrentUrl() + "#category/eddm/products/8507dc8e-7a6d-470c-b9f2-dd6286cf901d", "SellSheets link is wrong");
        String SellSheets_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[1]/ul/li[3]/a")).getText();
        so.assertEquals(SellSheets_getText, "Sell Sheets", "SellSheets sring is wrong");

    }

    @Test(dependsOnMethods = {"Login"}, groups = "Links")
    public void MajesticProducts() {

        SoftAssert so = new SoftAssert();

        boolean MajesticProducts_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[2]/button")).isEnabled();
        so.assertEquals(MajesticProducts_isEnable, true);
        String MajesticProducts_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[2]/button")).getText();
        so.assertEquals(MajesticProducts_getText, "Majestic Products", "Majestic Products string is wrong");

        boolean Akuafoil_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[1]/a")).isEnabled();
        so.assertEquals(Akuafoil_isEnable, true);
        String Akuafoil_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[1]/a")).getAttribute("href");
        so.assertEquals(Akuafoil_href, driver.getCurrentUrl() + "#category/majestic/products/16b74a6e-1487-47c7-90eb-a8c95ddba152", "Akuafoil link is wrong");
        String Akuafoil_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[1]/a")).getText();
        so.assertEquals(Akuafoil_getText, "Akuafoil", "Akuafoild string is wrong");

        boolean BrownKraftCards_isEnable = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[2]/a")).isEnabled();
        so.assertEquals(BrownKraftCards_isEnable, true);
        String BrownKraftcards_href = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[2]/a")).getAttribute("href");
        so.assertEquals(BrownKraftcards_href, driver.getCurrentUrl() + "#category/majestic/products/26a7c6e7-15f7-4e29-a3d6-40c2b1037f6a", "BrownKraftCards link is wrong");
        String BrownKraftcards_getText = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[2]/a")).getText();
        so.assertEquals(BrownKraftcards_getText, "Brown Kraft Cards", "Brown Kraft Cards is wrong");

        boolean EdgeCards_isEnable = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[3]/a")).isEnabled();
        so.assertEquals(EdgeCards_isEnable, true);
        String EdgeCards_href = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[3]/a")).getAttribute("href");
        so.assertEquals(EdgeCards_href, driver.getCurrentUrl() + "#category/majestic/products/b579b1ea-a1d2-45bb-afd9-bec57295d140", "EdgeCards link is wrong");
        String EdgeCards_getText = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[3]/a")).getText();
        so.assertEquals(EdgeCards_getText, "Edge Cards", "Edge Cards string is wrong");


        boolean FoilWorx_isEnable = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[4]/a")).isEnabled();
        so.assertEquals(FoilWorx_isEnable, true);
        String FoilWorx_href = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[4]/a")).getAttribute("href");
        so.assertEquals(FoilWorx_href, driver.getCurrentUrl() + "#category/majestic/products/3a7249ea-2244-44a3-8d44-4333dffe26b7", "FoildWorx link is wrong");
        String FoilWorx_getText = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[4]/a")).getText();
        so.assertEquals(FoilWorx_getText, "Foil Worx", "Foil worx string is wrong");


        boolean LusterCards_isEnable = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[5]/a")).isEnabled();
        so.assertEquals(LusterCards_isEnable, true);
        String LusterCards_href = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[5]/a")).getAttribute("href");
        so.assertEquals(LusterCards_href, driver.getCurrentUrl() + "#category/majestic/products/d3fcb50d-d083-42d9-9ac3-a168039ffb0a", "LusterCards link is wrong");
        String LusterCards_getText = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[5]/a")).getText();
        so.assertEquals(LusterCards_getText, "Luster Cards", "Luster Cards string is wrong");

        boolean NaturalCards_isEnable = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[6]/a")).isEnabled();
        so.assertEquals(NaturalCards_isEnable, true);
        String NaturalCards_href = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[6]/a")).getAttribute("href");
        so.assertEquals(NaturalCards_href, driver.getCurrentUrl() + "#category/majestic/products/26d754f4-1ff3-44ec-8f80-bb74371c4e3f", "NaturalCards link is wrong");
        String NaturalCards_getText = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[6]/a")).getText();
        so.assertEquals(NaturalCards_getText, "Natural Cards", "Natural Cards string is wrong");

        boolean PaintedEdgeCards_isEnable = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[7]/a")).isEnabled();
        so.assertEquals(PaintedEdgeCards_isEnable, true);
        String PaintedEdgeCards_href = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[7]/a")).getAttribute("href");
        so.assertEquals(PaintedEdgeCards_href, driver.getCurrentUrl() + "#category/majestic/products/85ff1bba-c250-40db-84a7-f58d5a8d72c5", "PaintededgeCards link is wrong");
        String PaintedEdgeCards_getText = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[7]/a")).getText();
        so.assertEquals(PaintedEdgeCards_getText, "Painted Edge Cards", "Painted edge Cards is wrong");

        boolean PearlCards_isEnable = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[8]/a")).isEnabled();
        so.assertEquals(PearlCards_isEnable, true);
        String PearlCards_href = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[8]/a")).getAttribute("href");
        so.assertEquals(PearlCards_href, driver.getCurrentUrl() + "#category/majestic/products/579ce415-37aa-44d3-ab23-0a8f68cb783c", "PearlCards link is wrong");
        String PearlCards_getText = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[8]/a")).getText();
        so.assertEquals(PearlCards_getText, "Pearl Cards", "Pearl Cards string is wrong");

        boolean RaisedFoil_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[9]/a")).isEnabled();
        so.assertEquals(RaisedFoil_isEnable, true);
        String RaisedFoil_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[9]/a")).getAttribute("href");
        so.assertEquals(RaisedFoil_href, driver.getCurrentUrl() + "#category/majestic/products/0d5ffea0-2e5f-41a7-a5b3-a851c9686f82", "RaisedSpotUV link is wrong");
        String RaisedFoil_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[9]/a")).getText();
        so.assertEquals(RaisedFoil_getText, "Raised Foil", "Raised Foil string is wrong");


        boolean RaisedSpotUV_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']//a[text()='Raised Spot UV']")).isEnabled();
        so.assertEquals(RaisedSpotUV_isEnable, true);
        String RaisedSpotUV_href = driver.findElement(By.xpath(".//*[@id='leftNav']//a[text()='Raised Spot UV']")).getAttribute("href");
        so.assertEquals(RaisedSpotUV_href, driver.getCurrentUrl() + "#category/majestic/products/e3745dee-4494-4317-8b97-9c8eb679893d", "RaisedSpotUV link is wrong");
        String RaisedSpotUV_getText = driver.findElement(By.xpath(".//*[@id='leftNav']//a[text()='Raised Spot UV']")).getText();
        so.assertEquals(RaisedSpotUV_getText, "Raised Spot UV", "Raised Spot UV string is wrong");

        boolean SilkCards_isEnable = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[11]/a")).isEnabled();
        so.assertEquals(SilkCards_isEnable, true);
        String SilkCards_href = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[11]/a")).getAttribute("href");
        so.assertEquals(SilkCards_href, driver.getCurrentUrl() + "#category/majestic/products/5f08f16e-2280-4727-a8bb-fdd936b9724c", "SilkCards link is wrong");
        String SilkCards_getText = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[11]/a")).getText();
        so.assertEquals(SilkCards_getText, "Silk Cards", "Silk Cards string is wrong");

        boolean PlatinumPearlCards_isEnable = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[12]/a")).isEnabled();
        so.assertEquals(PlatinumPearlCards_isEnable, true);
        String PlatinumPearlCards_href = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[12]/a")).getAttribute("href");
        so.assertEquals(PlatinumPearlCards_href, driver.getCurrentUrl() + "#category/majestic/products/a6937fba-45b9-4fb8-806c-7adca7c5b587", "PlatiniumPearlCards link is wrong");
        String PlatinumPearlCards_getText = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[12]/a")).getText();
        so.assertEquals(PlatinumPearlCards_getText, "Platinum Pearl Cards", "Platinum Pearl Cards string is wrong");

        boolean SuedeCards_isEnable = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[13]/a")).isEnabled();
        so.assertEquals(SuedeCards_isEnable, true);
        String SuedeCards_href = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[13]/a")).getAttribute("href");
        so.assertEquals(SuedeCards_href, driver.getCurrentUrl() + "#category/majestic/products/efd0c89e-e861-464e-a34e-b69dd55026b5", "SuedeCards link is wrong");
        String SuedeCards_getText = driver.findElement(By.xpath("./*//*[@id='leftNav']/nav/ul/li/ul/li[2]/ul/li[13]/a")).getText();
        so.assertEquals(SuedeCards_getText, "Suede Cards", "Suede Cards string is wrong");

        so.assertAll();

    }

    @Test(dependsOnMethods = {"Login"}, groups = "Links")
    public void Marketing() {

        SoftAssert so = new SoftAssert();

        boolean Marketing_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/button")).isEnabled();
        so.assertEquals(Marketing_isEnable, true);
        String MajesticProducts_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/button")).getText();
        so.assertEquals(MajesticProducts_getText, "Marketing", "Marketing string is wrong");

        boolean BusinessCards_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[1]/a")).isEnabled();
        so.assertEquals(BusinessCards_isEnable, true);
        String BusinessCards_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[1]/a")).getAttribute("href");
        so.assertEquals(BusinessCards_href, driver.getCurrentUrl() + "#category/marketing/products/04797b1f-f27b-492f-b19f-fcf88962dee3", "BusinessCards link is wrong");
        String BusinessCards_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[1]/a")).getText();
        so.assertEquals(BusinessCards_getText, "Business Cards", "business Cards string is wrong");

        boolean Postcards_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[2]/a")).isEnabled();
        so.assertEquals(Postcards_isEnable, true);
        String Postcards_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[2]/a")).getAttribute("href");
        so.assertEquals(Postcards_href, driver.getCurrentUrl() + "#category/marketing/products/6013b7ab-6911-4fb9-a8b6-6d83943f9637", "Postcards link is worng");
        String Postcards_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[2]/a")).getText();
        so.assertEquals(Postcards_getText, "Postcards", "Postcards string is wrong");

        boolean FlyerandBrochures_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[3]/a")).isEnabled();
        so.assertEquals(FlyerandBrochures_isEnable, true);
        String FlyerandBrochures_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[3]/a")).getAttribute("href");
        so.assertEquals(FlyerandBrochures_href, driver.getCurrentUrl() + "#category/marketing/products/b6002f8d-aaab-4fd7-bec0-5c1e11ad2a42", "FlyerandBrochures link is wrong");
        String FlyerandBrochures_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[3]/a")).getText();
        so.assertEquals(FlyerandBrochures_getText, "Flyers and Brochures", "Flyer and Brochures string is wrong");

        boolean AnnouncementCards_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[4]/a")).isEnabled();
        so.assertEquals(AnnouncementCards_isEnable, true);
        String AnnouncementCards_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[4]/a")).getAttribute("href");
        so.assertEquals(AnnouncementCards_href, driver.getCurrentUrl() + "#category/marketing/products/440ffa65-aa90-40cf-b131-81c1ab789eef", "AnnouncmentCards link is wrong");
        String AnnouncementCards_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[4]/a")).getText();
        so.assertEquals(AnnouncementCards_getText, "Announcement Cards", "AnnouncmentCards string is wrong");

        boolean Booklets_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[5]/a")).isEnabled();
        so.assertEquals(Booklets_isEnable, true);
        String Booklets_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[5]/a")).getAttribute("href");
        so.assertEquals(Booklets_href, driver.getCurrentUrl() + "#category/marketing/products/85c0d695-f038-49ba-9183-76bc34325390", "Booklet link is wrong");
        String Booklets_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[5]/a")).getText();
        so.assertEquals(Booklets_getText, "Booklets", "Booklet string is wrong");


        boolean Bookmarks_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[6]/a")).isEnabled();
        so.assertEquals(Bookmarks_isEnable, true);
        String Bookmarks_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[6]/a")).getAttribute("href");
        so.assertEquals(Bookmarks_href, driver.getCurrentUrl() + "#category/marketing/products/0d49b6c5-2431-4c33-af48-8da42cd5adc3", "Bookmarks link is wrong");
        String Bookmarks_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[6]/a")).getText();
        so.assertEquals(Bookmarks_getText, "Bookmarks", "bookmark string is wrong");

        boolean Calendars_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[7]/a")).isEnabled();
        so.assertEquals(Calendars_isEnable, true);
        String Calendars_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[7]/a")).getAttribute("href");
        so.assertEquals(Calendars_href, driver.getCurrentUrl() + "#category/marketing/products/80a9f6e8-23fe-4595-84a2-f686ed1b3b4f", "Calendars link is wrong");
        String Calendars_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[7]/a")).getText();
        so.assertEquals(Calendars_getText, "Calendars", "Calendars string is wrong");

        boolean CDandDVD_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[8]/a")).isEnabled();
        so.assertEquals(CDandDVD_isEnable, true);
        String CDandDVD_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[8]/a")).getAttribute("href");
        so.assertEquals(CDandDVD_href, driver.getCurrentUrl() + "#category/marketing/products/00cff56a-4011-4343-8699-d0197596950f", "CDandDVD link is wrong");
        String CDandDVD_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[8]/a")).getText();
        so.assertEquals(CDandDVD_getText, "CD and DVD", "CDandDVD string is wrong");

        boolean CounterCards_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[9]/a")).isEnabled();
        so.assertEquals(CounterCards_isEnable, true);
        String CounterCards_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[9]/a")).getAttribute("href");
        so.assertEquals(CounterCards_href, driver.getCurrentUrl() + "#category/marketing/products/21430071-a29f-4c77-8b6c-e405d0cdd115", "Countercards link is wrong");
        String CounterCards_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[9]/a")).getText();
        so.assertEquals(CounterCards_getText, "Counter Cards", "CounterCards string is wrong");

        boolean DoorHangers_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[10]/a")).isEnabled();
        so.assertEquals(DoorHangers_isEnable, true);
        String DoorHangers_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[10]/a")).getAttribute("href");
        so.assertEquals(DoorHangers_href, driver.getCurrentUrl() + "#category/marketing/products/8f737ee0-68f4-4ca4-b77d-97aab1e58a46", "DoorHangers link is wrong");
        String DoorHangers_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[10]/a")).getText();
        so.assertEquals(DoorHangers_getText, "Door Hangers", "Door hangers string is wrong");

        boolean Endurace_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[11]/a")).isEnabled();
        so.assertEquals(Endurace_isEnable, true);
        String Endurace_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[11]/a")).getAttribute("href");
        so.assertEquals(Endurace_href, driver.getCurrentUrl() + "#category/marketing/products/1d8c922b-eadf-43f8-b725-7336fbad517e", "Endurace link is wrong");
        String Endurace_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[11]/a")).getText();
        so.assertEquals(Endurace_getText, "Endurace", "Endurace string is worng");

        boolean Envelopes_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[12]/a")).isEnabled();
        so.assertEquals(Envelopes_isEnable, true);
        String Envelopes_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[12]/a")).getAttribute("href");
        so.assertEquals(Envelopes_href, driver.getCurrentUrl() + "#category/marketing/products/a8d00249-a790-41a3-aafa-22350fd96bf9", "Envelopes link is wrong");
        String Envelopes_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[12]/a")).getText();
        so.assertEquals(Envelopes_getText, "Envelopes", "Envelopes string is wrong");

        boolean EventTickets_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[13]/a")).isEnabled();
        so.assertEquals(EventTickets_isEnable, true);
        String EventTickets_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[13]/a")).getAttribute("href");
        so.assertEquals(EventTickets_href, driver.getCurrentUrl() + "#category/marketing/products/1cbed957-4230-4abc-9d89-55b77b5f3129", "EventTickets link is wrong");
        String EventTickets_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[13]/a")).getText();
        so.assertEquals(EventTickets_getText, "Event Tickets", "EventTickets string is wrong");

        boolean GreetingCards_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[14]/a")).isEnabled();
        so.assertEquals(GreetingCards_isEnable, true);
        String GreetingCards_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[14]/a")).getAttribute("href");
        so.assertEquals(GreetingCards_href, driver.getCurrentUrl() + "#category/marketing/products/39f94046-b863-4d40-87d1-9b13ef2bb57c", "GreetingCards link is wrong");
        String GreetingCards_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[14]/a")).getText();
        so.assertEquals(GreetingCards_getText, "Greeting Cards", "GreetingCards string is wrong");

        boolean HangTags_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[15]/a")).isEnabled();
        so.assertEquals(HangTags_isEnable, true);
        String HangTags_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[15]/a")).getAttribute("href");
        so.assertEquals(HangTags_href, driver.getCurrentUrl() + "#category/marketing/products/63321afb-34d6-4c52-9138-d6fb82d7efc8", "HangTags link is wrong");
        String HangTags_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[15]/a")).getText();
        so.assertEquals(HangTags_getText, "Hang Tags", "HangTags string is wrong");

        boolean HeaderCards_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[16]/a")).isEnabled();
        so.assertEquals(HeaderCards_isEnable, true);
        String HeaderCards_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[16]/a")).getAttribute("href");
        so.assertEquals(HeaderCards_href, driver.getCurrentUrl() + "#category/marketing/products/6777ce57-5ad2-4428-a92a-77af189cb4a3", "HeaderCards link is wrong");
        String HeaderCards_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[16]/a")).getText();
        so.assertEquals(HeaderCards_getText, "Header Cards", "HeaderCards string is wrong");

        boolean Letterheads_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[17]/a")).isEnabled();
        so.assertEquals(Letterheads_isEnable, true);
        String Letterheads_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[17]/a")).getAttribute("href");
        so.assertEquals(Letterheads_href, driver.getCurrentUrl() + "#category/marketing/products/cc8f98e4-157e-434e-a3b9-f12f37a915fb", "Letterheads link is wrong");
        String Letterheads_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[17]/a")).getText();
        so.assertEquals(Letterheads_getText, "Letterheads", "Letterheads string is wrong");

        boolean LinenUncoated_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[18]/a")).isEnabled();
        so.assertEquals(LinenUncoated_isEnable, true);
        String LinenUncoated_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[18]/a")).getAttribute("href");
        so.assertEquals(LinenUncoated_href, driver.getCurrentUrl() + "#category/marketing/products/4365a267-97f2-465f-9a27-06a846fe4cbe", "LinenUncoated link is wrong ");
        String LinenUncoated_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[18]/a")).getText();
        so.assertEquals(LinenUncoated_getText, "Linen Uncoated", "LineUncoated string is wrong");

        boolean Magnets_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[19]/a")).isEnabled();
        so.assertEquals(Magnets_isEnable, true);
        String Magnets_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[19]/a")).getAttribute("href");
        so.assertEquals(Magnets_href, driver.getCurrentUrl() + "#category/marketing/products/abb6d9b6-9544-401f-9004-1c1fd4669a24", "Magnets link is wrong");
        String Magnets_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[19]/a")).getText();
        so.assertEquals(Magnets_getText, "Magnets", "Magnets string is wrong");

        boolean NCRForms_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[21]/a")).isEnabled();
        so.assertEquals(NCRForms_isEnable, true);
        String NCRForms_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[21]/a")).getAttribute("href");
        so.assertEquals(NCRForms_href, driver.getCurrentUrl() + "#category/marketing/products/02933044-ac99-418f-971e-bd97637778df", "NCRforms link is wrong");
        String NCRForms_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[21]/a")).getText();
        so.assertEquals(NCRForms_getText, "NCR Forms", "NCRforms string is wrong");

        boolean Notepads_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[22]/a")).isEnabled();
        so.assertEquals(Notepads_isEnable, true);
        String Notepads_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[22]/a")).getAttribute("href");
        so.assertEquals(Notepads_href, driver.getCurrentUrl() + "#category/marketing/products/3073e732-2191-439e-a8f2-3a6312b5a8fe", "Notepads link is wrong");
        String Notepads_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[22]/a")).getText();
        so.assertEquals(Notepads_getText, "Notepads", "Notepads string is wrong");

        boolean PlasticCards_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[23]/a")).isEnabled();
        so.assertEquals(PlasticCards_isEnable, true);
        String PlasticCards_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[23]/a")).getAttribute("href");
        so.assertEquals(PlasticCards_href, driver.getCurrentUrl() + "#category/marketing/products/b91f6a29-d91a-492e-893f-bf37e84efbed", "PlasticCards link is wrong");
        String PlasticCards_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[23]/a")).getText();
        so.assertEquals(PlasticCards_getText, "Plastic Cards", "Plasticcards string is wrong");

        boolean Posters_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[24]/a")).isEnabled();
        so.assertEquals(Posters_isEnable, true);
        String Posters_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[24]/a")).getAttribute("href");
        so.assertEquals(Posters_href, driver.getCurrentUrl() + "#category/marketing/products/faf9a4bb-ae62-49a4-a1ef-5428a4046600", "Posters link is wrong");
        String Posters_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[24]/a")).getText();
        so.assertEquals(Posters_getText, "Posters", "Posters string is wrong");

        boolean PresentationFolders_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[25]/a")).isEnabled();
        so.assertEquals(PresentationFolders_isEnable, true);
        String PresentationFolders_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[25]/a")).getAttribute("href");
        so.assertEquals(PresentationFolders_href, driver.getCurrentUrl() + "#category/marketing/products/a6ea0aad-97a7-4a97-8f95-33fef4b0120c", "Presentationfolders link is wrong");
        String PresentationFolders_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[25]/a")).getText();
        so.assertEquals(PresentationFolders_getText, "Presentation Folders", "PresentationFolder string is wrong");

        boolean RackCards_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[26]/a")).isEnabled();
        so.assertEquals(RackCards_isEnable, true);
        String RackCards_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[26]/a")).getAttribute("href");
        so.assertEquals(RackCards_href, driver.getCurrentUrl() + "#category/marketing/products/9f01d78c-0bce-451f-ae43-efd731f66442", "RackCards link is wrong");
        String RackCards_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[26]/a")).getText();
        so.assertEquals(RackCards_getText, "Rack Cards", "RackCards string is wrong");

        boolean SellSheets_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[27]/a")).isEnabled();
        so.assertEquals(SellSheets_isEnable, true);
        String SellSheets_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[27]/a")).getAttribute("href");
        so.assertEquals(SellSheets_href, driver.getCurrentUrl() + "#category/marketing/products/566d8ea3-d575-4f36-ad19-14184d8e910a", "SellSheets link is wrong");
        String SellSheets_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[27]/a")).getText();
        so.assertEquals(SellSheets_getText, "Sell Sheets", "SellSheets string is wrong");

        boolean TableTents_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[28]/a")).isEnabled();
        so.assertEquals(TableTents_isEnable, true);
        String TableTents_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[28]/a")).getAttribute("href");
        so.assertEquals(TableTents_href, driver.getCurrentUrl() + "#category/marketing/products/a8d5f3d7-3881-4007-94e5-83a71ee1c1e2", "TableTents link is wrong");
        String TableTents_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[28]/a")).getText();
        so.assertEquals(TableTents_getText, "Table Tents", "TableTents string is wrong");

        boolean TearOffCards_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[29]/a")).isEnabled();
        so.assertEquals(TearOffCards_isEnable, true);
        String TearOffCards_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[29]/a")).getAttribute("href");
        so.assertEquals(TearOffCards_href, driver.getCurrentUrl() + "#category/marketing/products/ba94f7b7-440b-4689-a56f-8cc01a4cc901", "TearOfCards link is wrong");
        String TearOffCards_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[29]/a")).getText();
        so.assertEquals(TearOffCards_getText, "Tear Off Cards", "TearOfCards string is wrong");

        boolean TradingCards_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[30]/a")).isEnabled();
        so.assertEquals(TradingCards_isEnable, true);
        String TradingCards_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[30]/a")).getAttribute("href");
        so.assertEquals(TradingCards_href, driver.getCurrentUrl() + "#category/marketing/products/8dde350e-d235-485b-9fef-7b3c27a2e0cb", "TradingCards link is worng");
        String TradingCards_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[30]/a")).getText();
        so.assertEquals(TradingCards_getText, "Trading Cards", "TradingCards string is wrong");

        boolean WindowClings_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[31]/a")).isEnabled();
        so.assertEquals(WindowClings_isEnable, true);
        String WindowClings_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[31]/a")).getAttribute("href");
        so.assertEquals(WindowClings_href, driver.getCurrentUrl() + "#category/marketing/products/19db1fd1-0b22-4f73-ad0e-8364d861b24c", "WindowClings link is wrong");
        String WindowClings_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[31]/a")).getText();
        so.assertEquals(WindowClings_getText, "Window Clings", "WindowClings string is wrong");

        boolean Lenticular_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[32]/a")).isEnabled();
        so.assertEquals(Lenticular_isEnable, true);
        String Lenticular_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[32]/a")).getAttribute("href");
        so.assertEquals(Lenticular_href, driver.getCurrentUrl() + "#category/marketing/products/dad2fe6d-7cea-47f4-9d6b-67033dd04465", "Lenticular link is wrong");
        String Lenticular_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[3]/ul/li[32]/a")).getText();
        so.assertEquals(Lenticular_getText, "4D Lenticular", "Lenticular string is wrong");

        so.assertAll();


    }

    @Test(dependsOnMethods = {"Login"}, groups = "Links")
    public void LargeFormatProducts() {

        SoftAssert so = new SoftAssert();

        boolean LargeFormatProducts_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/button")).isEnabled();
        so.assertEquals(LargeFormatProducts_isEnable, true,"Large format product title is not available");
        String LargeFormatProducts_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/button")).getText();
        so.assertEquals(LargeFormatProducts_getText, "Large Format Products", "Large Format String is wrong");

        boolean Signs_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[1]/a")).isEnabled();
        so.assertEquals(Signs_isEnable, true,"Sign is not available");
        String Signs_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[1]/a")).getAttribute("href");
        so.assertEquals(Signs_href, driver.getCurrentUrl() + "#category/large-format/products/2d047d2d-fedf-489b-8c60-e58141e4c0a9", "Signs link is wrong");
        String Signs_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[1]/a")).getText();
        so.assertEquals(Signs_getText, "Signs", "Signs string is wrong");

        boolean IndoorBanners_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[2]/a")).isEnabled();
        so.assertEquals(IndoorBanners_isEnable, true,"IndoorBanners is not available ");
        String IndoorBanners_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[2]/a")).getAttribute("href");
        so.assertEquals(IndoorBanners_href, driver.getCurrentUrl() + "#category/large-format/products/5936584c-dc67-4251-bc1c-3c6d8f059b26","IndoorBanner link is wrong");
        String Indoorbanners_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[2]/a")).getText();
        so.assertEquals(Indoorbanners_getText, "Indoor Banners","IndoorBanners string is wrong");


        boolean OutdoorBanners_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[3]/a")).isEnabled();
        so.assertEquals(OutdoorBanners_isEnable, true,"OutdoorBanners is not available");
        String OutdoorBanners_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[3]/a")).getAttribute("href");
        so.assertEquals(OutdoorBanners_href, driver.getCurrentUrl() + "#category/large-format/products/ab8752c1-b291-444d-8d4a-ddc7d03948b7","OutdoorBanners link is wrong");
        String OutdoorBanners_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[3]/a")).getText();
        so.assertEquals(OutdoorBanners_getText, "Outdoor Banners","OutdoorBanners string is wrong");

        boolean AdhesiveVinyl_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[4]/a")).isEnabled();
        so.assertEquals(AdhesiveVinyl_isEnable, true,"AdhesiveVinyls is not available");
        String AdhesiveVinyl_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[4]/a")).getAttribute("href");
        so.assertEquals(AdhesiveVinyl_href, driver.getCurrentUrl() + "#category/large-format/products/0d06c579-ad63-4878-9aa6-b7efdab454d8","AdhesiveVinyl link is wrong");
        String AdhesiveVinyl_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[4]/a")).getText();
        so.assertEquals(AdhesiveVinyl_getText, "Adhesive Vinyl","Adhesive Vinyls string is wrong");


        boolean BacklitPosters_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[5]/a")).isEnabled();
        so.assertEquals(BacklitPosters_isEnable, true,"BacklitPosters is not available");
        String BacklitPosters_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[5]/a")).getAttribute("href");
        so.assertEquals(BacklitPosters_href, driver.getCurrentUrl() + "#category/large-format/products/9de42d8a-13e0-4300-a8f0-e406c52583d8","Backlitposters link is wrong");
        String BacklitPosters_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[5]/a")).getText();
        so.assertEquals(BacklitPosters_getText, "Backlit Posters","BacklitPosters string is wrong");

        boolean BannerStands_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[6]/a")).isEnabled();
        so.assertEquals(BannerStands_isEnable, true,"BannerStands is not available");
        String BannerStands_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[6]/a")).getAttribute("href");
        so.assertEquals(BannerStands_href, driver.getCurrentUrl() + "#category/large-format/products/0bcbf188-0e08-41de-aa3b-88037fd28f48","bannerStands link is wrong");
        String BannerStands_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[6]/a")).getText();
        so.assertEquals(BannerStands_getText, "Banner Stands","BannerStands string is wrong");

        boolean CarMagnets_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[7]/a")).isEnabled();
        so.assertEquals(CarMagnets_isEnable, true,"carMagnets is not available");
        String CarMagnets_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[7]/a")).getAttribute("href");
        so.assertEquals(CarMagnets_href, driver.getCurrentUrl() + "#category/large-format/products/574d690c-d4e9-4992-96e2-4418182132cb","CarMagnets link is wrong");
        String CarMagnets_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[7]/a")).getText();
        so.assertEquals(CarMagnets_getText, "Car Magnets","CarMagnets string is wrong");

        boolean FabricBanners_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[8]/a")).isEnabled();
        so.assertEquals(FabricBanners_isEnable, true,"FabricBanners is not available");
        String FabricBanners_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[8]/a")).getAttribute("href");
        so.assertEquals(FabricBanners_href, driver.getCurrentUrl() + "#category/large-format/products/b3689d57-6ed7-4c5e-8fa9-a648f3245b8b","FabricBanners link is wrong");
        String FabricBanners_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[8]/a")).getText();
        so.assertEquals(FabricBanners_getText, "Fabric Banners","Fabric Banners string is wrong");

        boolean Flags_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[9]/a")).isEnabled();
        so.assertEquals(Flags_isEnable, true,"Flags is not available");
        String Flags_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[9]/a")).getAttribute("href");
        so.assertEquals(Flags_href, driver.getCurrentUrl() + "#category/large-format/products/7b9ff24a-021e-42a9-b5ae-a7c8e6e990ac","Flags link is wrong");
        String Flags_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[9]/a")).getText();
        so.assertEquals(Flags_getText, "Flags","Flags string is wrong");

        boolean LargePosters_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[10]/a")).isEnabled();
        so.assertEquals(LargePosters_isEnable, true,"LargePosters is not available");
        String LargePosters_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[10]/a")).getAttribute("href");
        so.assertEquals(LargePosters_href, driver.getCurrentUrl() + "#category/large-format/products/4b067691-80c0-4522-b3fd-db66c88eccd4","LargePosters link is wrong");
        String LargePosters_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[10]/a")).getText();
        so.assertEquals(LargePosters_getText, "Large Posters","Large Posters string is wrong");

        boolean MountedCanvas_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[11]/a")).isEnabled();
        so.assertEquals(MountedCanvas_isEnable, true,"mountedCanvas is not available");
        String MountedCanvas_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[11]/a")).getAttribute("href");
        so.assertEquals(MountedCanvas_href, driver.getCurrentUrl() + "#category/large-format/products/bfb850d3-b179-401f-ac34-7c981b8d351b","MountedCanvas link is wrong");
        String MountedCanvas_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[11]/a")).getText();
        so.assertEquals(MountedCanvas_getText, "Mounted Canvas","Mounted Canvas string is wrong");

        boolean SildewalkSigns_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[12]/a")).isEnabled();
        so.assertEquals(SildewalkSigns_isEnable, true,"SildewalkSigns is not available");
        String SildewalkSigns_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[12]/a")).getAttribute("href");
        so.assertEquals(SildewalkSigns_href, driver.getCurrentUrl() + "#category/large-format/products/81adc462-e080-4d06-8deb-4ea5497e06ef","SildewalkSigns link is wrong");
        String SildewalkSigns_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[12]/a")).getText();
        so.assertEquals(SildewalkSigns_getText, "Sidewalk Signs","SildewalkSigns string is wrong");

        boolean TableCovers_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[13]/a")).isEnabled();
        so.assertEquals(TableCovers_isEnable, true,"Tablecovers is not available");
        String TableCovers_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[13]/a")).getAttribute("href");
        so.assertEquals(TableCovers_href, driver.getCurrentUrl() + "#category/large-format/products/eb03532c-3514-45d0-bb09-4a0f85ad7086","TableCovers link is wrong");
        String TableCovers_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[13]/a")).getText();
        so.assertEquals(TableCovers_getText, "Table Covers","Table Covers string is wrong");

        boolean TabletopDisplays_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[14]/a")).isEnabled();
        so.assertEquals(TabletopDisplays_isEnable, true,"TabletopDisplays is not available");
        String TabletopDisplays_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[14]/a")).getAttribute("href");
        so.assertEquals(TabletopDisplays_href, driver.getCurrentUrl() + "#category/large-format/products/8f53f156-7c47-41b9-8fb5-4044f7831164","TabletopDisplays link is wrong");
        String TabletopDisplays_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[14]/a")).getText();
        so.assertEquals(TabletopDisplays_getText, "Tabletop Displays","Tabletop Displays string is wrong");

        boolean WindowGraphics_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[15]/a")).isEnabled();
        so.assertEquals(WindowGraphics_isEnable, true,"WindowGraphics is not available");
        String WindowGraphics_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[15]/a")).getAttribute("href");
        so.assertEquals(WindowGraphics_href, driver.getCurrentUrl() + "#category/large-format/products/52ac73cc-129c-454a-b9eb-58280a6335d9","WindowGraphics link is wrong");
        String WindowGraphics_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[4]/ul/li[15]/a")).getText();
        so.assertEquals(WindowGraphics_getText, "Window Graphics","Window Graphics string is wrong");

        so.assertAll();
    }


    @Test(dependsOnMethods = {"Login"}, groups = "Links")
    public void PaperBoardBoxes() {

        SoftAssert so = new SoftAssert();

        boolean PaperBoardBoxes_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/button")).isEnabled();
        so.assertEquals(PaperBoardBoxes_isEnable, true,"PaperBoardBoxes is not avialable");
        String PaperBoardBoxes_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/button")).getText();
        so.assertEquals(PaperBoardBoxes_getText, "Paper Board Boxes","PaperBoardboxes string is wrong");

        boolean CustomBoxes_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[1]/a")).isEnabled();
        so.assertEquals(CustomBoxes_isEnable, true,"CustomBoxes is not available");
        String WindowGraphics_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[1]/a")).getAttribute("href");
        so.assertEquals(WindowGraphics_href, driver.getCurrentUrl() + "#category/box/products/928776c5-9b51-4259-a172-77f88ce045b4","WindowGraphics link is wrong");
        String WindowGraphics_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[1]/a")).getText();
        so.assertEquals(WindowGraphics_getText, "Custom Boxes");

        boolean BusinessCardBoxes_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[2]/a")).isEnabled();
        so.assertEquals(BusinessCardBoxes_isEnable, true,"BusinessCardBoxes is not avialable");
        String BusinessCardBoxes_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[2]/a")).getAttribute("href");
        so.assertEquals(BusinessCardBoxes_href, driver.getCurrentUrl() + "#category/box/products/2bb7359c-2aab-41ed-b271-e2564d90aaed","BusinessCard link is wrong");
        String BusinessCardBoxes_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[2]/a")).getText();
        so.assertEquals(BusinessCardBoxes_getText, "Business Card Boxes","BusinessCardBoxes string is wrong");

        boolean CubeBoxes_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[3]/a")).isEnabled();
        so.assertEquals(CubeBoxes_isEnable, true,"Cubeboxes is not available");
        String CubeBoxes_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[3]/a")).getAttribute("href");
        so.assertEquals(CubeBoxes_href, driver.getCurrentUrl() + "#category/box/products/7216a133-2d8b-4fd0-aeef-27d7e947d8b4","cubeBoxes link is wrong");
        String CubeBoxes_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[3]/a")).getText();
        so.assertEquals(CubeBoxes_getText, "Cube Boxes","CubeBoxes string is wrong");

        boolean GolfBallBoxes_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[4]/a")).isEnabled();
        so.assertEquals(GolfBallBoxes_isEnable, true,"GolfBallBoxes is not avialable");
        String GolfBallBoxes_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[4]/a")).getAttribute("href");
        so.assertEquals(GolfBallBoxes_href, driver.getCurrentUrl() + "#category/box/products/7623d4b7-3046-4478-b194-fe91c4692a61","GolfBallBoxes link is wrong ");
        String GolfBallBoxes_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[4]/a")).getText();
        so.assertEquals(GolfBallBoxes_getText, "Golf Ball Boxes","golfBallBoxes string is wrong");

        boolean PillowBoxes_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[5]/a")).isEnabled();
        so.assertEquals(PillowBoxes_isEnable, true,"Pillowboxes is not available");
        String PillowBoxes_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[5]/a")).getAttribute("href");
        so.assertEquals(PillowBoxes_href, driver.getCurrentUrl() + "#category/box/products/6db7c17d-10e1-4c76-82e8-58b373d9b1ca","PillowBoxes link is wrong");
        String PillowBoxes_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[5]/a")).getText();
        so.assertEquals(PillowBoxes_getText, "Pillow Boxes","PillowBoxes string is wrong");

        boolean PrintTrimBoxes_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[6]/a")).isEnabled();
        so.assertEquals(PrintTrimBoxes_isEnable, true,"PrintTrimBoxes is not available");
        String PrintTrimBoxes_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[6]/a")).getAttribute("href");
        so.assertEquals(PrintTrimBoxes_href, driver.getCurrentUrl() + "#category/box/products/61c3729f-8316-4172-8acb-c3b53deb4520","PrinttrimBoxes link is wrong");
        String PrintTrimBoxes_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[6]/a")).getText();
        so.assertEquals(PrintTrimBoxes_getText, "Print & Trim Boxes","PrinttrimBoxes string is wrong");

        boolean RollEndTucktopBoxes_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[7]/a")).isEnabled();
        so.assertEquals(RollEndTucktopBoxes_isEnable, true,"rollEndTuckBoxes is not available");
        String RollEndTucktopBoxes_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[7]/a")).getAttribute("href");
        so.assertEquals(RollEndTucktopBoxes_href, driver.getCurrentUrl() + "#category/box/products/18390258-62f0-44ee-9453-035bb9a8b7b9","RollEndTuckboxes link is wrong");
        String RollEndTucktopBoxes_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[7]/a")).getText();
        so.assertEquals(RollEndTucktopBoxes_getText, "Roll End Tuck Top Boxes","RollendTuckBoxes string is wrong");

        boolean SalesPresentationBoxes_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[8]/a")).isEnabled();
        so.assertEquals(SalesPresentationBoxes_isEnable, true,"SalesPresentation Boxes is not available");
        String SalesPresentationBoxes_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[8]/a")).getAttribute("href");
        so.assertEquals(SalesPresentationBoxes_href, driver.getCurrentUrl() + "#category/box/products/364f9ed1-cddf-40b2-a0d4-584aab17646e","SalesPresentation Boxes link is wrong");
        String SalesPresentationBoxes_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[8]/a")).getText();
        so.assertEquals(SalesPresentationBoxes_getText, "Sales Presentation Boxes","SalesPresentation string is wrong");

        boolean WineBoxes_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[9]/a")).isEnabled();
        so.assertEquals(WineBoxes_isEnable, true,"wineBoxes is not available");
        String WineBoxes_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[9]/a")).getAttribute("href");
        so.assertEquals(WineBoxes_href, driver.getCurrentUrl() + "#category/box/products/bd9ec8f3-0277-4d70-ba47-57554e278acc","WineBoxes link is wrong");
        String WineBoxes_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[5]/ul/li[9]/a")).getText();
        so.assertEquals(WineBoxes_getText, "Wine Boxes","Wine Boxes string is wrong");


    }

    @Test(dependsOnMethods = {"Login"}, groups = "Links")
    public void LabelsStickers() {

        SoftAssert so = new SoftAssert();

        boolean LabelsStickers_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[6]/button")).isEnabled();
        so.assertEquals(LabelsStickers_isEnable, true,"LabelSrickers is not available");
        String LabelsStickers_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[6]/button")).getText();
        so.assertEquals(LabelsStickers_getText, "Labels & Stickers","Labels & Stickers string is wrong");

        boolean BumperStickers_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[6]/ul/li[1]/a")).isEnabled();
        so.assertEquals(BumperStickers_isEnable, true,"BumperStickers is not available");
        String BumperStickers_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[6]/ul/li[1]/a")).getAttribute("href");
        so.assertEquals(BumperStickers_href, driver.getCurrentUrl() + "#category/rls/products/254b8b9e-61e7-434d-aee6-7d3f82ec2e47","bumperStickers link is wrong");
        String BumperStickers_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[6]/ul/li[1]/a")).getText();
        so.assertEquals(BumperStickers_getText, "Bumper Stickers","BumperStickers string is wrong");

        boolean RollLabels_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[6]/ul/li[2]/a")).isEnabled();
        so.assertEquals(RollLabels_isEnable, true,"RollLabels is not available ");
        String RollLabels_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[6]/ul/li[2]/a")).getAttribute("href");
        so.assertEquals(RollLabels_href, driver.getCurrentUrl() + "#category/rls/products/8cb3991e-6823-450c-b673-b03ef8480492","RollLabels link is wrong");
        String RollLabels_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[6]/ul/li[2]/a")).getText();
        so.assertEquals(RollLabels_getText, "Roll Labels","RollLabels string is wrong");

        boolean Stickers_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[6]/ul/li[3]/a")).isEnabled();
        so.assertEquals(Stickers_isEnable, true,"Stickers is not available");
        String Stickers_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[6]/ul/li[3]/a")).getAttribute("href");
        so.assertEquals(Stickers_href, driver.getCurrentUrl() + "#category/rls/products/9c280003-79cd-4def-961b-5e98536fda86","Stickers link is wrong");
        String Stickers_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[6]/ul/li[3]/a")).getText();
        so.assertEquals(Stickers_getText, "Stickers","Stickers string is wrong");
        so.assertAll();

    }

    @Test(dependsOnMethods = {"Login"})
    public void PromotionalProducts() {

        SoftAssert so = new SoftAssert();


        boolean PromotionalProducts_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[7]/button")).isEnabled();
        so.assertEquals(PromotionalProducts_isEnable, true,"Promotional Procust is not available");
        String PromotionalProducts_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[7]/button")).getText();
        so.assertEquals(PromotionalProducts_getText, "Promotional Products","Promotional Procusts string is not available");

        boolean Buttons_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[7]/ul/li[1]/a")).isEnabled();
        so.assertEquals(Buttons_isEnable, true,"Buttons is not available");
        String Buttons_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[7]/ul/li[1]/a")).getAttribute("href");
        so.assertEquals(Buttons_href, driver.getCurrentUrl() + "#category/pos/products/2c419d4d-2c20-4ab6-8bf6-6ead09c1880e","Buttons link is not available");
        String Buttons_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[7]/ul/li[1]/a")).getText();
        so.assertEquals(Buttons_getText, "Buttons","Buttons string is wrong");

        boolean Mugs_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[7]/ul/li[2]/a")).isEnabled();
        so.assertEquals(Mugs_isEnable, true,"Mugs is not available");
        String Mugs_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[7]/ul/li[2]/a")).getAttribute("href");
        so.assertEquals(Mugs_href, driver.getCurrentUrl() + "#category/pos/products/3151b07b-7c31-4a7e-894d-f50cdd47e1ca","Mugs link is worng");
        String Mugs_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[7]/ul/li[2]/a")).getText();
        so.assertEquals(Mugs_getText, "Mugs","Mugs string is wrong");

        boolean TShirts_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[7]/ul/li[3]/a")).isEnabled();
        so.assertEquals(TShirts_isEnable, true,"TShirts is not available");
        String TShirts_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[7]/ul/li[3]/a")).getAttribute("href");
        so.assertEquals(TShirts_href, driver.getCurrentUrl() + "#category/pos/products/7157555f-b119-4f81-bd2d-eaa117ff28a0","Tshirts link is wrong");
        String TShirts_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[7]/ul/li[3]/a")).getText();
        so.assertEquals(TShirts_getText, "T-Shirts","Tshirts string is wrong");

        boolean ToteBags_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[7]/ul/li[4]/a")).isEnabled();
        so.assertEquals(ToteBags_isEnable, true,"ToteBags is not available");
        String ToteBags_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[7]/ul/li[4]/a")).getAttribute("href");
        so.assertEquals(ToteBags_href, driver.getCurrentUrl() + "#category/pos/products/173d464a-cbaa-449a-8eeb-64ae617b7fe9","ToteBags link is wrong");
        String ToteBags_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[7]/ul/li[4]/a")).getText();
        so.assertEquals(ToteBags_getText, "Tote Bags","ToteBags string is wrong");

        so.assertAll();

    }


    @Test(dependsOnMethods = {"Login"}, groups = "Links")
    public void Services() {

        SoftAssert so = new SoftAssert();

        boolean PromotionalProducts_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[8]/button")).isEnabled();
        so.assertEquals(PromotionalProducts_isEnable, true,"Promotional Products is not available");
        String PromotionalProducts_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[8]/button")).getText();
        so.assertEquals(PromotionalProducts_getText, "Services", "promotional Products string is wrong");

        boolean EDDM_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[8]/ul/li[1]/a")).isEnabled();
        so.assertEquals(EDDM_isEnable, true,"EDDM is not available");
        String EDDM_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[8]/ul/li[1]/a")).getAttribute("href");
        so.assertEquals(EDDM_href, driver.getCurrentUrl() + "#eddm","EDDM link is wrong ");
        String EDDM_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[8]/ul/li[1]/a")).getText();
        so.assertEquals(EDDM_getText, "EDDM","EDDM string is wrong ");


        boolean PackagingEstimates_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[8]/ul/li[2]/a")).isEnabled();
        so.assertEquals(PackagingEstimates_isEnable, true,"Packaging Estimates is not available");
        String PackagingEstimates_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[8]/ul/li[2]/a")).getAttribute("href");
        so.assertEquals(PackagingEstimates_href, driver.getCurrentUrl() + "#estimate","Packaging Estimates link is wrong ");
        String PackagingEstimates_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[8]/ul/li[2]/a")).getText();
        so.assertEquals(PackagingEstimates_getText, "Packaging Estimates","Packaging Estimates string is wrong");

        boolean GangRunEstimates_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[8]/ul/li[3]/a")).isEnabled();
        so.assertEquals(GangRunEstimates_isEnable, true,"GangRunEstimates is not available");
        String GangRunEstimates_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[8]/ul/li[3]/a")).getAttribute("href");
        so.assertEquals(GangRunEstimates_href, "https://trade.4over.com/orders/estimates.php","GangRunEstimates link is wrong");
        String GangRunEstimates_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[8]/ul/li[3]/a")).getText();
        so.assertEquals(GangRunEstimates_getText, "Gang-Run Estimates","GangRunEstimates string is wrong");

        boolean SampleRequests_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[8]/ul/li[4]/a")).isEnabled();
        so.assertEquals(SampleRequests_isEnable, true,"SampleRequest is not available");
        String SampleRequests_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[8]/ul/li[4]/a")).getAttribute("href");
        so.assertEquals(SampleRequests_href, "https://trade.4over.com/api.php?callback=/products/sample-request/","SampleRequests link is wrong");
        String SampleRequests_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[8]/ul/li[4]/a")).getText();
        so.assertEquals(SampleRequests_getText, "Sample Requests","SampleRequests string is wrong");
        so.assertAll();

    }




    @Test(dependsOnMethods = {"Login"}, groups = "Links")
    public void Marketplace() {

        SoftAssert so = new SoftAssert();

        boolean PromotionalProducts_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[9]/button")).isEnabled();
        so.assertEquals(PromotionalProducts_isEnable, true,"Promotional Products is not available");
        String PromotionalProducts_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[9]/button")).getText();
        so.assertEquals(PromotionalProducts_getText, "Marketplace", "Promotional Products string is wrong");

        boolean Marketplace_isEnable = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[9]/ul/li/a")).isEnabled();
        so.assertEquals(Marketplace_isEnable, true,"MarketPlace is not available");
        String Marketplace_href = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[9]/ul/li/a")).getAttribute("href");
        so.assertEquals(Marketplace_href, "https://trade.4over.com/api.php?callback=products/marketplace/", "Marketplace link is wrong");
        String Marketplace_getText = driver.findElement(By.xpath(".//*[@id='leftNav']/nav/ul/li/ul/li[9]/ul/li/a")).getText();
        so.assertEquals(Marketplace_getText, "Marketplace", "Marketplace string is wrong");
        so.assertAll();

    }


    @AfterMethod
    public void After_Method(ITestResult testResult){

        if (testResult.getStatus() == ITestResult.FAILURE){

            System.out.println("Failed:  " +
                    "  " + testResult.getMethod().getMethodName());
        }
        if (testResult.getStatus() == ITestResult.SUCCESS){

            System.out.println("Success:  " + testResult.getMethod().getMethodName());
        }

    }
    @AfterClass
    public void After_Class() throws InterruptedException {
        Thread.sleep(500);
        driver.close();


    }

}
