import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by tadeh on 1/6/17.
 */
public class Login_Validation {


    WebDriver driver;
    setDataDriven data;
    private String browser;
    private String platform;



    @BeforeClass
    public void BeforeClass() throws IOException {

        data = new setDataDriven();
        data.props();
        browser = data.getBrowser();
        platform=data.getPlatform();

        switch(browser) {

            case "firefox": {
                if (platform.equals("windows")) {
                    System.setProperty("webdriver.gecko.driver", "C:\\projects\\Ultra_Regrestion\\src\\test\\java\\drivers\\windows\\geckodriver.exe");
                } else {
                    System.setProperty("webdriver.gecko.driver", "./src/test/java/drivers/mac/geckodriver");
                }
                driver = new FirefoxDriver();
                break;
            }
            case "chrome": {
                if (platform.equals("windows")) {
                    System.setProperty("webdriver.chrome.driver", "C:\\projects\\Ultra_Regrestion\\src\\test\\java\\drivers\\windows\\chromedriver.exe");
                } else {
                    System.setProperty("webdriver.chrome.driver", "./src/test/java/drivers/mac/chromedriver");
                }
                driver = new ChromeDriver();
                break;
            }

            case "safari": {
            }



        }

        driver.get(data.getEnvironment());
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MILLISECONDS);

    }

    @Test(description = "LogIn Validation")
    public void Login() throws IOException, InterruptedException {

        long start = System.currentTimeMillis();
        WebElement Username = driver.findElement(By.xpath(".//input[@id=\"txtEmail\"]"));
        Username.sendKeys(data.getUsername());
        Thread.sleep(2000);
        WebElement Password = driver.findElement(By.xpath(".//input[@id=\"txtPassword\"]"));
        Password.sendKeys(data.getPassword());
        Thread.sleep(2000);
        WebElement SignIn = driver.findElement(By.xpath(".//input[@id=\"btnLogin\"]"));
        SignIn.click();
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//button[@id=\"btnCart\"]")));
        long finish = System.currentTimeMillis();
        long OverallTime = finish - start;

        System.out.println("User Successfully Logged-in" + "Total time for login -  " + OverallTime + " ms");


    }

    @Test(dependsOnMethods = {"Login"},description = "LogOut validation")
    public void LogOut() {

        WebElement Logout = driver.findElement(By.xpath("//button[@id=\"logout\"]"));
        Logout.click();
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//input[@id=\"txtEmail\"]")));

        System.out.println("User Successfully Logged-out");
    }


    @AfterClass
    public void After_Class() throws InterruptedException {
        Thread.sleep(500);
        driver.close();
    }
}



