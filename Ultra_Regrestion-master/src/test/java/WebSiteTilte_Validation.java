import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Created by tadeh on 1/11/17.
 */
public class WebSiteTilte_Validation {

    WebDriver driver;
    setDataDriven data;
    private String browser;
    private String platform;


    @BeforeClass
    public void SetDataDriven() throws IOException {


        data = new setDataDriven();
        data.props();
        browser = data.getBrowser();
        platform=data.getPlatform();



        switch(browser) {

            case "firefox": {
                if (platform.equals("windows")) {
                    System.setProperty("webdriver.gecko.driver", "C:\\projects\\Ultra_Regrestion\\src\\test\\java\\drivers\\windows\\geckodriver.exe");
                } else {
                    System.setProperty("webdriver.gecko.driver", "./src/test/java/drivers/mac/geckodriver");
                }
                driver = new FirefoxDriver();
                break;
            }
            case "chrome": {
                if (platform.equals("windows")) {
                    System.setProperty("webdriver.chrome.driver", "C:\\projects\\Ultra_Regrestion\\src\\test\\java\\drivers\\windows\\chromedriver.exe");
                } else {
                    System.setProperty("webdriver.chrome.driver", "./src/test/java/drivers/mac/chromedriver");
                }
                driver = new ChromeDriver();
                break;
            }

            case "safari": {
            }

        }

        driver.get(data.getEnvironment());
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10,TimeUnit.SECONDS);
    }


    @Test
    public void webSite_Tilte(){

        String title = driver.getTitle();

        Assert.assertEquals("4over, Inc. | Super Trade Printer",title);
    }

    @AfterMethod
    public void After_Method(ITestResult testResult){

        if (testResult.getStatus() == ITestResult.FAILURE){

            System.out.println("Failed:  " +
                    "  " + testResult.getMethod().getMethodName());
        }
        if (testResult.getStatus() == ITestResult.SUCCESS){

            System.out.println("Success:  " + testResult.getMethod().getMethodName());
        }

    }

    @AfterClass
    public void After_Class() {

        driver.close();
    }

}
