import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by tadeh on 2/8/17.
 */
public class PageFooter {

    private WebDriver driver;
    private setDataDriven data;
    private String browser;
    private String platform;



    @BeforeClass
    public void BeforeClass() throws IOException {

        data = new setDataDriven();
        data.props();
        browser = data.getBrowser();
        platform = data.getPlatform();


        switch(browser) {

            case "firefox": {
                if (platform.equals("windows")) {
                    System.setProperty("webdriver.gecko.driver", "C:\\projects\\Ultra_Regrestion\\src\\test\\java\\drivers\\windows\\geckodriver.exe");
                } else {
                    System.setProperty("webdriver.gecko.driver", "./src/test/java/drivers/mac/geckodriver");
                }
                driver = new FirefoxDriver();
                break;
            }
            case "chrome": {
                if (platform.equals("windows")) {
                    System.setProperty("webdriver.chrome.driver", "C:\\projects\\Ultra_Regrestion\\src\\test\\java\\drivers\\windows\\chromedriver.exe");
                } else {
                    System.setProperty("webdriver.chrome.driver", "./src/test/java/drivers/mac/chromedriver");
                }
                driver = new ChromeDriver();
                break;
            }

            case "safari": {
            }

        }

        driver.get(data.getEnvironment());
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(40,TimeUnit.SECONDS);

    }


    @Test
    public void MainMenu() {

        SoftAssert so = new SoftAssert();

        String MainMenu = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[1]/h5")).getText();
        so.assertEquals(MainMenu, "MAIN MENU","MainMenu string is wrong");
        String Home1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[1]/ul/li[1]/a")).getAttribute("href");
        so.assertEquals(Home1, driver.getCurrentUrl() + "#","Home link is wrong");
        String Home2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[1]/ul/li[1]/a")).getText();
        so.assertEquals(Home2, "Home","Home string is wrong");
        String MyAccount1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[1]/ul/li[2]/a")).getAttribute("href");
        so.assertEquals(MyAccount1, "https://trade.4over.com/api.php?callback=/orders/account_profile.php","MyAcount link is wrong");
        String MyAccount2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[1]/ul/li[2]/a")).getText();
        so.assertEquals(MyAccount2, "My Account","MyAccount string is wrong");
        String ViewCart1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[1]/ul/li[3]/a")).getAttribute("href");
        so.assertEquals(ViewCart1, driver.getCurrentUrl() + "#cart","ViewCart link is wrong");
        String ViewCart2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[1]/ul/li[3]/a")).getText();
        so.assertEquals(ViewCart2, "View Cart","ViewCart string is wrong");
        String AboutUs1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[1]/ul/li[4]/a")).getAttribute("href");
        so.assertEquals(AboutUs1, driver.getCurrentUrl() + "#about","AboutUs link is wrong");
        String AboutUs2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[1]/ul/li[4]/a")).getText();
        so.assertEquals(AboutUs2, "About Us","AboutUs string is wrong");
        String Equipment1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[1]/ul/li[5]/a")).getAttribute("href");
        so.assertEquals(Equipment1, driver.getCurrentUrl() + "#equipment","Equipment link is wrong");
        String Equipment2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[1]/ul/li[5]/a")).getText();
        so.assertEquals(Equipment2, "Equipment","Equipment string is wrong");
        String Careers1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[1]/ul/li[6]/a")).getAttribute("href");
        so.assertEquals(Careers1, driver.getCurrentUrl() + "#careers","Career link is wrong");
        String Careers2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[1]/ul/li[6]/a")).getText();
        so.assertEquals(Careers2, "Careers","Career string is wrong");
        so.assertAll();
    }

    @Test
    public void Resources() {

        SoftAssert so = new SoftAssert();

        String Resources = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/h5")).getText();
        so.assertEquals(Resources, "RESOURCES","RESOURCES string is wrong");
        String EveryDoorDirectmailFAQs1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[1]/a")).getAttribute("href");
        so.assertEquals(EveryDoorDirectmailFAQs1, driver.getCurrentUrl() + "#faqs/eddm","EveryDoorDirectmail link is wrong");
        String EveryDoorDirectmailFAQs2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[1]/a")).getText();
        so.assertEquals(EveryDoorDirectmailFAQs2, "Every Door Direct Mail FAQs","EveryDoordirectMail string is wrong");
        String PackagingFAQs1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[2]/a")).getAttribute("href");
        so.assertEquals(PackagingFAQs1, driver.getCurrentUrl() + "#faqs/paper-board-boxes","PackagignFAQ link is wrong");
        String PackagingFAQ2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[2]/a")).getText();
        so.assertEquals(PackagingFAQ2, "Packaging FAQs","Packaging FAQ string is wrong");
        String RollLabelsFAQ1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[3]/a")).getAttribute("href");
        so.assertEquals(RollLabelsFAQ1, driver.getCurrentUrl() + "#faqs/labels-&-stickers","RollLabelsFAQ link is wrong");
        String RollLabelsFAQ2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[3]/a")).getText();
        so.assertEquals(RollLabelsFAQ2, "Roll Labels FAQs","RollLabelFAQ string is wrong");
        String AllOtherFAQs1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[4]/a")).getAttribute("href");
        so.assertEquals(AllOtherFAQs1, "https://trade.4over.com/api.php?callback=/resources.php","AllOther FAQ link is wrong");
        String AllOtherFAQs2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[4]/a")).getText();
        so.assertEquals(AllOtherFAQs2, "All Other FAQs","AllOtherFAQ string is wrong");
        String TemplatesDies1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[5]/a")).getAttribute("href");
        so.assertEquals(TemplatesDies1, driver.getCurrentUrl() + "#templates","Templates Die link is wrong");
        String TemplatesDies2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[5]/a")).getText();
        so.assertEquals(TemplatesDies2, "Templates / Dies","Templates Dies string is wrong");
        String MyMarketing1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[6]/a")).getAttribute("href");
        so.assertEquals(MyMarketing1, driver.getCurrentUrl() + "#mymarketing","MyMarketing link is wrong");
        String MyMarketing2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[6]/a")).getText();
        so.assertEquals(MyMarketing2, "MyMarketing","MyMarketing string is wrong");
        String TurnaroundTimes1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[7]/a")).getAttribute("href");
        so.assertEquals(TurnaroundTimes1, driver.getCurrentUrl() + "#turnaround-times","TurnaroundTimes link is wrong");
        String TurnaroundTimes2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[7]/a")).getText();
        so.assertEquals(TurnaroundTimes2, "Turnaround Times","TurnaroundTime string is wrong");
        String ShippingTime1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[8]/a")).getAttribute("href");
        so.assertEquals(ShippingTime1, driver.getCurrentUrl() + "#shipping-times","ShippingTime link is wrong");
        String ShippingTime2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[8]/a")).getText();
        so.assertEquals(ShippingTime2, "Shipping Time","shippingTime string is wrong");
        String SiteRequirments1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[9]/a")).getAttribute("href");
        so.assertEquals(SiteRequirments1, driver.getCurrentUrl() + "#site-requirements","Site Requirment link is wrong");
        String Siterequirments2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[2]/ul/li[9]/a")).getText();
        so.assertEquals(Siterequirments2, "Site Requirements","Site Requirment string is wrong");
        so.assertAll();
    }

    @Test
    public void Legal() {

        SoftAssert so = new SoftAssert();

        boolean Legal1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[3]/h5")).isEnabled();
        so.assertEquals(Legal1, true,"Legal is not enable");
        String Legal2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[3]/h5")).getText();
        so.assertEquals(Legal2, "LEGAL","Legal string is wrong");
        boolean ReturnPolicy1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[3]/ul/li[1]/a")).isEnabled();
        so.assertEquals(ReturnPolicy1, true,"Return policy is not available");
        String ReturnPolicy2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[3]/ul/li[1]/a")).getAttribute("href");
        so.assertEquals(ReturnPolicy2, driver.getCurrentUrl() + "#legal/return-policy","Return Plicy link is wrong");
        String ReturnPolicy3 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[3]/ul/li[1]/a")).getText();
        so.assertEquals(ReturnPolicy3, "Return Policy","Return Policy string is wrong");
        boolean ShippingPolicy1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[3]/ul/li[2]/a")).isEnabled();
        so.assertEquals(ShippingPolicy1, true,"Shipping policy is not avialable");
        String ShippingPolicy2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[3]/ul/li[2]/a")).getAttribute("href");
        so.assertEquals(ShippingPolicy2, driver.getCurrentUrl() + "#legal/shipping-policy","Shipping Policy link is wrong");
        String ShippingPolicy3 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[3]/ul/li[2]/a")).getText();
        so.assertEquals(ShippingPolicy3,"Shipping Policy", "Shipping Policy string is wrong");
        boolean TermsAndConditions1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[3]/ul/li[3]/a")).isEnabled();
        so.assertEquals(TermsAndConditions1, true,"Terms and Condirion is not avialable");
        String TermsAndConditions2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[3]/ul/li[3]/a")).getAttribute("href");
        so.assertEquals(TermsAndConditions2, driver.getCurrentUrl() + "#legal/terms-and-conditions","Terms and Condition link is wrong");
        String TermsAndConditions3 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[3]/ul/li[3]/a")).getText();
        so.assertEquals(TermsAndConditions3, "Terms and Conditions","Terms and condition string is wrong");
        boolean PrivacyStatment1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[3]/ul/li[4]/a")).isEnabled();
        so.assertEquals(PrivacyStatment1, true,"privacy Statment is not avialable");
        String PrivacyStatment2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[3]/ul/li[4]/a")).getAttribute("href");
        so.assertEquals(PrivacyStatment2, driver.getCurrentUrl() + "#legal/privacy-statement","Privacy statment link is wrong");
        String PrivacyStatment3 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[3]/ul/li[4]/a")).getText();
        so.assertEquals(PrivacyStatment3, "Privacy Statement","Privacy statment string is wrong");
        boolean CASupplyChainsAct1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[3]/ul/li[5]/a")).isEnabled();
        so.assertEquals(CASupplyChainsAct1, true,"CA Supply Chain is not available");
       String CASupplyChainsAct2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[3]/ul/li[5]/a")).getAttribute("href");
        so.assertEquals(CASupplyChainsAct2, driver.getCurrentUrl() + "#legal/ca-supply","CA Supply Chain link is wrong");
        String CASupplyChainsAct3 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[3]/ul/li[5]/a")).getText();
        so.assertEquals(CASupplyChainsAct3, "CA Supply Chains Act","CA Supply Chain string is wrong");
        so.assertAll();
    }

    @Test
    public void Contact() {

        SoftAssert so = new SoftAssert();

        boolean Contact1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[4]/h5")).isEnabled();
        so.assertEquals(Contact1, true,"Contact is not available");
        String Contact2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[4]/h5")).getText();
        so.assertEquals(Contact2, "CONTACT","Contact string is wrong");
        boolean chat1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/section[4]/ul/li[1]/a")).isEnabled();
        so.assertEquals(chat1, true,"chat is not available");
        so.assertAll();
    }

    @Test
    public void SocialMediaLinks() {

        SoftAssert so = new SoftAssert();


        boolean StartFollowingUsToday1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[1]/div/p")).isEnabled();
        so.assertEquals(StartFollowingUsToday1, true,"Start Following US Today text is not available");
        String StartFollowingUsToday2 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[1]/div/p")).getText();
        so.assertEquals(StartFollowingUsToday2, "Start following us today","Start Following us Today string is wrong");

        boolean Twitter1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[1]/div/ul/li[1]/a")).isEnabled();
        so.assertEquals(Twitter1, true,"Twitter is not available");
        String Twitter3 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[1]/div/ul/li[1]/a")).getAttribute("href");
        so.assertEquals(Twitter3, "https://twitter.com/4over","Twitter link is wrong");
        String Twitter4 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[1]/div/ul/li[1]/a")).getText();
        so.assertEquals(Twitter4, "Follow us on Twitter","Twitter string is wrong");

        boolean facebook1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[1]/div/ul/li[2]/a")).isEnabled();
        so.assertEquals(facebook1, true,"Facebook is not available");
        String facebook3 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[1]/div/ul/li[2]/a")).getAttribute("href");
        so.assertEquals(facebook3, "https://www.facebook.com/4OverLLC","Facebook link is wrong");
        String facebook4 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[1]/div/ul/li[2]/a")).getText();
        so.assertEquals(facebook4, "Visit us on Facebook","Facebook string is wrong");

        boolean google1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[1]/div/ul/li[3]/a")).isEnabled();
        so.assertEquals(google1, true,"Google is not available");
        String google3 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[1]/div/ul/li[3]/a")).getAttribute("href");
        so.assertEquals(google3, "https://plus.google.com/u/0/111944129666129578777/posts","Google link is wrong");
        String google4 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[1]/div/ul/li[3]/a")).getText();
        so.assertEquals(google4, "Visit us on Google Plus","Google string is wrong");

        boolean youtube1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[1]/div/ul/li[4]/a")).isEnabled();
        so.assertEquals(youtube1, true,"Youtube is not available");
        String youtube3 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[1]/div/ul/li[4]/a")).getAttribute("href");
        so.assertEquals(youtube3, "https://www.youtube.com/user/4overinc","Youtube link is wrong");
        String youtube4 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[1]/div/ul/li[4]/a")).getText();
        so.assertEquals(youtube4, "Visit us on YouTube","youtube string is wrong");
        so.assertAll();
    }

    @Test
    public void CopyRightLogo() {

        SoftAssert so = new SoftAssert();

        boolean Logo1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/p")).isEnabled();
        so.assertEquals(Logo1, true," Copyright Logo is not availabe");
        String Logo3 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/p")).getText();
        so.assertEquals(Logo3, "© 2017 4OVER, LLC.","Copyright string is wrong");
        so.assertAll();
    }

    @Test
    public void CreditCardsLogos() {

        SoftAssert so = new SoftAssert();

        boolean CreditCards1 = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/img")).isEnabled();
        so.assertEquals(CreditCards1, true,"CreditCard is not available");
        String Creditcardswidth = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/img")).getAttribute("width");
        so.assertEquals(Creditcardswidth, "160","CreditCards icon width got changed");
        String Creditcardsheight = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/img")).getAttribute("height");
        so.assertEquals(Creditcardsheight, "24","creditCards height got changed");
        String CreditcardsSrc = driver.findElement(By.xpath(".//*[@id='main-footer']/div/footer/div[2]/div/img")).getAttribute("src");
        so.assertEquals(CreditcardsSrc, "https://4cdn.4over.com/img/creditcards.png","CreditCards logo's source link is worng");
        so.assertAll();

    }
    @AfterMethod
    public void After_Method(ITestResult testResult){

        if (testResult.getStatus() == ITestResult.FAILURE){

            System.out.println("Failed:  " +
                    "  " + testResult.getMethod().getMethodName());
        }
        if (testResult.getStatus() == ITestResult.SUCCESS){

            System.out.println("Success:  " + testResult.getMethod().getMethodName());
        }

    }

    @AfterClass
    protected void After_Class() throws InterruptedException {

        Thread.sleep(500);
        driver.close();
    }
}
